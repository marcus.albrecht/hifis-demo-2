#!/bin/bash
#
# HIFIS Demo #2 Runtime Script
# 2022-09-01
# Marcus W. Albrecht
#
# https://gitlab.hzdr.de/marcus.albrecht/hifis-demo-2/-/raw/main/src/HIFIS-demo-2.sh?inline=false -O HIFIS-demo-2.sh

# Set script behaviour

#set -x
#trap 'read -p "#####   $BASH_COMMAND"' DEBUG

# Set environment variables

export WORKDIR="/opt"
export OIDC_SOCK=`/bin/ls -rt /tmp/oidc-forward-* 2> /dev/null | tail -n 1`
export OIDC_TOKEN="hifis"
export DCACHE_URL="https://dcache-demo.desy.de:2443"
export DCACHE_DIR="/Demonstrators/HIFIS/2022 demo"

# Setup workspace

mkdir -p $WORKDIR/usr $WORKDIR/src $WORKDIR/dCache $WORKDIR/results
cd $WORKDIR

Help()
{
   # Display Help
   echo "Installs, configures rclone and miniconda + HIP album catalog and performs HIP image analyses using HIFIS services."
   echo
   echo "Syntax: bash HIFIS-demo-2.sh [-e|h|i|r|p|x]"
   echo "options:"
   echo "e     Initialize miniconda virtual environment."
   echo "h     Print this Help."
   echo "i     Install system packages and prerequisites."
   echo "r     Activate miniconda virtual environment perform HIP image analysis."
   echo "p     Purge $WORKDIR and remove local package repositories."
   echo "x     Clean up results from GitLab and dCache."
   echo
}

Clean()
{
  # Clone GitLab Repository if non-existent otherwise pull and discard local changes

  if [ -d "$WORKDIR/hifis-demo-2" ] 
    then
      cd "$WORKDIR/hifis-demo-2"
      git reset --hard HEAD
      git clean -f -d
      git pull
    else
      git clone https://gitlab.hzdr.de/marcus.albrecht/hifis-demo-2.git
  fi

  # remove results.png from GitLab packages

  cd $WORKDIR/hifis-demo-2/public
  git rm results.png
  git commit -m "Delete HIFIS Demo #2 results.png"
  git push origin main

  # Mount dCache directory

  if [ ! -d "$WORKDIR/dCache/STLs" ] 
    then
      screen -Sdm rclone $WORKDIR/usr/rclone-v1.59.0-linux-amd64/rclone mount "DESY:$DCACHE_DIR" "$WORKDIR/dCache"
      sleep 5
  fi

  # Remove results files from dCache

  rm $WORKDIR/dCache/STLs/* $WORKDIR/dCache/PNGs/*

  pkill -u `whoami` rclone
}

Purge()
{
  # Clone GitLab Repository if non-existent otherwise pull and discard local changes

  if [ -d "$WORKDIR/hifis-demo-2" ] 
    then
      cd "$WORKDIR/hifis-demo-2"
      git reset --hard HEAD
      git clean -f -d
      git pull
    else
      git clone https://gitlab.hzdr.de/marcus.albrecht/hifis-demo-2.git
  fi

  # remove results.png from GitLab packages

  cd $WORKDIR/hifis-demo-2/public
  git rm results.png
  git commit -m "Delete HIFIS Demo #2 results.png"
  git push origin main

  # Mount dCache directory

  if [ ! -d "$WORKDIR/dCache/STLs" ] 
    then
      screen -Sdm rclone $WORKDIR/usr/rclone-v1.59.0-linux-amd64/rclone mount "DESY:$DCACHE_DIR" "$WORKDIR/dCache"
      sleep 5
  fi

  # Remove results files from dCache

  rm $WORKDIR/dCache/STLs/* $WORKDIR/dCache/PNGs/*

  # Kill processes and unmount dCache directory

  pkill -u `whoami` rclone
  pkill -u `whoami` oidc-agent

  # Clean up $WORKDIR

  rm -rf $WORKDIR/usr $WORKDIR/src $WORKDIR/dCache $WORKDIR/results

  # Clean up local package repositories and configs

  rm -rf $HOME/.album $HOME/.cache $HOME/.conda $HOME/.config
}

InstallEnv()
{
  # Install and Configure rclone

  wget https://downloads.rclone.org/v1.59.0/rclone-v1.59.0-linux-amd64.zip -O src/rclone-v1.59.0-linux-amd64.zip
  unzip src/rclone-v1.59.0-linux-amd64.zip -d usr/

  mkdir -p ~/.config/rclone
  echo -e "[DESY]\ntype = webdav\nurl = $DCACHE_URL\nvendor = other\nbearer_token_command = oidc-token $OIDC_TOKEN" >> ~/.config/rclone/rclone.conf

  # Install Miniconda

  wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $WORKDIR/src/Miniconda3-latest-Linux-x86_64.sh
  bash $WORKDIR/src/Miniconda3-latest-Linux-x86_64.sh -b -f -p $WORKDIR/usr/Miniconda3
  eval "$(/$WORKDIR/usr/Miniconda3/bin/conda shell.bash hook)"
  conda init

  # Install album, HIP catalog, mesh-to-pixel and blender-render

  conda env create -f https://gitlab.com/album-app/album/-/raw/main/album.yml --prefix $WORKDIR/usr/album
  conda activate /opt/usr/album
  album add-catalog https://gitlab.com/album-app/catalogs/helmholtz-imaging
  album add-catalog https://gitlab.com/album-app/catalogs/helmholtz-imaging-dev
  album install de.mdc-berlin:pixel-to-mesh:0.1.0
#  album install de.mdc-berlin:blender-render-stl:0.1.0-SNAPSHOT
  album install de.mdc-berlin:blender-import-meshes:0.1.0-SNAPSHOT
  album install de.mdc-berlin:blender-style-glass:0.1.0-SNAPSHOT
  conda init bash
}

InstallSys()
{
  # Add oidc-agent key and repository

  sudo apt-key adv --keyserver hkp://pgp.surfnet.nl --recv-keys ACDFB08FDC962044D87FF00B512839863D487A87
  sudo lsb_release -c | awk '{print "add-apt-repository \"deb http://repo.data.kit.edu/debian/"$2" ./\""}' | sh
  sudo apt-get update

  # Install mandatory packages
 
  sudo apt-get install python3-venv libffi-dev software-properties-common oidc-agent rsync screen unzip
}

Run()
{
  # Kill any orphaned rclone processes

  pkill -u `whoami` rclone

  # Show oidc token information

  echo "#####   oidc-token $OIDC_TOKEN"
  oidc-token $OIDC_TOKEN
  echo "oidc-token $OIDC_TOKEN | cut -d. -f2 | base64 -d | jq ."
  oidc-token $OIDC_TOKEN | cut -d. -f2 | base64 -d | jq .

  # Activate conda environment

  eval "$(/$WORKDIR/usr/Miniconda3/bin/conda shell.bash hook)"
  echo "#####   eval \"\$(/$WORKDIR/usr/Miniconda3/bin/conda shell.bash hook)\""

  # Breakpoints from here on

  trap 'read -p "#####   $BASH_COMMAND"' DEBUG

  conda activate $WORKDIR/usr/album

  # Mount dCache directory

  screen -Sdm rclone $WORKDIR/usr/rclone-v1.59.0-linux-amd64/rclone mount "DESY:$DCACHE_DIR" "$WORKDIR/dCache"

  # Run STL file generation

  cd $WORKDIR/dCache/TIFs

  trap - DEBUG

  echo -e "#####   for TIF in *.tif\n#####   do\n#####     album run de.mdc-berlin:pixel-to-mesh:0.1.0 --gauss_sigma 1 --gauss_threshold 1 --mesh_threshold 0.2 --input \$TIF --output $WORKDIR/results/\`basename \$TIF .tif\`.stl\n#####   done"

  for TIF in *.tif
  do
    album run de.mdc-berlin:pixel-to-mesh:0.1.0 --gauss_sigma 1 --gauss_threshold 1 --mesh_threshold 0.2 --mesh_step_size 2 --input $TIF --output $WORKDIR/results/`basename $TIF .tif`.stl
  done

  trap 'read -p "#####   $BASH_COMMAND"' DEBUG

  # Run STL to Blender conversion and copy stl files to dCache directory

  cd $WORKDIR/results
  rsync -avP $WORKDIR/results/*.stl $WORKDIR/dCache/STLs
  album run de.mdc-berlin:blender-import-meshes:0.1.0-SNAPSHOT --input $WORKDIR/results --output_rendering $WORKDIR/results/results_mesh.png --output_blend $WORKDIR/results/results_mesh.blend --decimate_ratio 0.42
  album run de.mdc-berlin:blender-style-glass:0.1.0-SNAPSHOT --input $WORKDIR/results/results_mesh.blend --output_rendering $WORKDIR/results/results.png  --output_blend $WORKDIR/results/results.blend
  rsync -avP $WORKDIR/results/*.png $WORKDIR/dCache/PNGs

  # Clone GitLab Repository if non-existent otherwise pull and discard local changes

  trap - DEBUG

  echo -e "#####   cd \$WORKDIR/hifis-demo-2/public\n#####   git add *.png\n#####     git commit -m "Upload HIFIS Demo #2 Rendering Results"\n#####   git push origin main"

  cd $WORKDIR

  if [ -d "$WORKDIR/hifis-demo-2" ] 
    then
      cd "$WORKDIR/hifis-demo-2"
      git reset --hard HEAD
      git clean -f -d
      git pull
    else
      git clone https://gitlab.hzdr.de/marcus.albrecht/hifis-demo-2.git
  fi

  # Copy results data into repository

  rsync -avP $WORKDIR/results/*.png $WORKDIR/hifis-demo-2/public

  # Add, commit and push to HZDR GitLab

  cd $WORKDIR/hifis-demo-2/public
  git add *.png
  git commit -m "Upload HIFIS Demo #2 Rendering Results"
  git push origin main
}

while getopts ":ehirpx" option
do
   case $option in
      e) # Initialize miniconda virtual env
         InstallEnv
         exit;;
      h) # Display help
         Help
         exit;;
      i) # Install system packages and prerequisites
         InstallSys
         exit;;
      r) # Activate virtual env, run oidc-agent and run HIP image processing
         Run
         exit;;
      p) # Purge everything
         Purge
         exit;;
      x) # Clean up repositories
         Clean
         exit;;
      \?) # Invalid option
         Help
         exit;;
   esac
done

if [[ $# -eq 0 ]]
then
    Help
    exit 0
fi
