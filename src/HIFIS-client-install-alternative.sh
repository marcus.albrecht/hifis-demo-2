
#!/bin/bash
#
# HIFIS Client Install
# 2022-09-01
# Marcus W. Albrecht
#
# https://gitlab.hzdr.de/marcus.albrecht/hifis-demo-2/-/raw/main/src/HIFIS-client-install.sh?inline=false -O HIFIS-client-install.sh

# Set script behaviour

#set -x
#trap 'read -p "#####   $BASH_COMMAND"' DEBUG

# Set environment variables

export WORKDIR="$HOME"
export VENV_NAME="mccli"
export OIDC_ENCRYPTION_PW=""
export OIDC_TOKEN="hifis"
export REMOTE_IP="a32621-dev"
export REMOTE_PORT="8080"

cd $WORKDIR

Help()
{
   # Display Help
   echo "Installs, configures and runs oidc-agent and connects to a remote host using mccli."
   echo
   echo "Syntax: bash HIFIS-client-install.sh [-c|e|h|i|r|s|x]"
   echo "options:"
   echo "c     Activate python virtual environment, run oidc-agent and connect to remote host. (w/o token generation)"
   echo "e     Initialize python virtual environment."
   echo "h     Print this Help."
   echo "i     Install system packages and prerequisites."
   echo "r     Activate python virtual environment, run oidc-agent, generate token and connect to remote host. (CLI)"
   echo "s     Activate python virtual environment, run oidc-agent, generate token and connect to remote host. (X11)"
   echo "x     Clean Up python virtual environment and oidc-agent files."
   echo
}

CONNECT()
{
  # Stop oidc-agent

  pkill oidc-agent

  # Start oidc-agent

  eval `oidc-agent`
  echo "#####   eval \`oidc-agent\`"

  # Breakpoints from here on
  
  trap 'read -p "#####   $BASH_COMMAND"' DEBUG

  # Activate virtual environment

  source $VENV_NAME/bin/activate
  
  # Add existing token to oidc-agent

  oidc-add --pw-env $OIDC_TOKEN

  # Connet and forward OIDC Token

  mccli ssh -R /tmp/oidc-forward-$RANDOM:$OIDC_SOCK --oidc $OIDC_TOKEN $REMOTE_IP --mc-endpoint http://$REMOTE_IP:$REMOTE_PORT
}

INSTALL_ENV()
{
  # Breakpoints from here on

  trap 'read -p "#####   $BASH_COMMAND"' DEBUG

  # Create virtual environment

  python3 -m venv $VENV_NAME

  # Activate virtual environment

  source $VENV_NAME/bin/activate

  # Update pip

  pip3 install --upgrade pip

  # Install wheel package as a prerequisite

  pip3 install wheel

  # Install mccli package

  pip3 install mccli
}

INSTALL_SYS()
{
  # Add oidc-agent key and repository

  sudo apt-key adv --keyserver hkp://pgp.surfnet.nl --recv-keys ACDFB08FDC962044D87FF00B512839863D487A87
  sudo lsb_release -c | awk '{print "add-apt-repository \"deb http://repo.data.kit.edu/debian/"$2" ./\""}' | sh
  sudo apt-get update

  # Install mandatory packages
 
  sudo apt-get install python3-venv libffi-dev software-properties-common oidc-agent
}

RUN_CLI()
{
  # Stop oidc-agent

  pkill oidc-agent

  # Start oidc-agent

  eval `oidc-agent`
  echo "#####   eval \`oidc-agent\`"

  # Breakpoints from here on
  
  trap 'read -p "#####   $BASH_COMMAND"' DEBUG

  # Activate virtual environment

  source $VENV_NAME/bin/activate
  
  # Issue token generation

  oidc-gen --flow=code --pub --no-url-call --no-webserver --pw-env --scope "openid profile email offline_access eduperson_entitlement eduperson_scoped_affiliation eduperson_unique_id" --iss https://login-dev.helmholtz.de/oauth2/ $OIDC_TOKEN

  # Paste generated url into codeExchange

  read -p "Please enter URL: " URL
  oidc-gen --pw-env --codeExchange="$URL"

  # Connet and forward OIDC Token

  mccli ssh -R /tmp/oidc-forward-$RANDOM:$OIDC_SOCK --oidc $OIDC_TOKEN $REMOTE_IP --mc-endpoint http://$REMOTE_IP:$REMOTE_PORT
}

RUN_X11()
{
  # Stop oidc-agent

  pkill oidc-agent

  # Start oidc-agent

  eval `oidc-agent`
  echo "#####   eval \`oidc-agent\`"

  # Breakpoints from here on
  
  trap 'read -p "#####   $BASH_COMMAND"' DEBUG

  # Activate virtual environment

  source $VENV_NAME/bin/activate
  
  # Issue token generation

  oidc-gen --pw-env --scope "openid profile email offline_access eduperson_entitlement eduperson_scoped_affiliation eduperson_unique_id" --iss https://login-dev.helmholtz.de/oauth2/ $OIDC_TOKEN

  # Paste generated url into codeExchange

  #read -p "Please enter URL: " URL
  #oidc-gen --pw-env --codeExchange="$URL"

  # Connet and forward OIDC Token

  mccli ssh -R /tmp/oidc-forward-$RANDOM:$OIDC_SOCK --oidc $OIDC_TOKEN $REMOTE_IP --mc-endpoint http://$REMOTE_IP:$REMOTE_PORT
}

CLEAN()
{
  # Stop oidc-agent

  pkill oidc-agent

  # Remove oidc token

  rm -rf $HOME/.oidc-agent/$OIDC_TOKEN $HOME/.config/oidc-agent/$OIDC_TOKEN

  # Remove python virtual environment

  rm -rf $WORKDIR/$VENV_NAME
}

while getopts ":cehirsx" option
do
   case $option in
      c) # Connect only
         CONNECT
         exit;;
      e) # Initialize virtual env
         INSTALL_ENV
         exit;;
      h) # Display help
         Help
         exit;;
      i) # Install system packages and prerequisites
         INSTALL_SYS
         exit;;
      r) # Activate virtual env, run oidc-agent and connect to remote host (CLI)
         RUN_CLI
         exit;;
      s) # Activate virtual env, run oidc-agent and connect to remote host (X11)
         RUN_X11
         exit;;
      x) # Clean up
         CLEAN
         exit;;
      \?) # Invalid option
         Help
         exit;;
   esac
done

if [[ $# -eq 0 ]]
then
    Help
    exit 0
fi
